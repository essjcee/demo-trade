DROP DATABASE IF EXISTS trading_rest_api;
CREATE DATABASE trading_rest_api;
USE trading_rest_api;
DROP TABLE IF EXISTS stockdata;

CREATE TABLE stockdata(
id int auto_increment primary key,
date date,
stockTicker varchar(10),
price double,
quantity int,
buyOrSell varchar(10),
statusCode int
);
-- LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/Data.csv'
-- INTO TABLE stockdata
-- FIELDS TERMINATED BY ',' 
-- ENCLOSED BY '"'
-- LINES TERMINATED BY '\n';
LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/TestingData.csv'
INTO TABLE stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n';


DROP TABLE IF EXISTS stockprices;
CREATE TABLE stockprices(
id int auto_increment primary key,
stockTicker varchar(50),
tickerName varchar(60),
price double
);
INSERT INTO stockprices(stockTicker,tickerName, price) values ("Citi",'C',73);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Wells fargo','WFC',49.93);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Goldman Sachs','GS',413.12);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('JP Morgan','JPM',160.92);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Morgan Stanley','MS',104.04);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Bank of America','BAC',42.25);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Apple','AAPL',148);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Microsoft','MSFT',302.01);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Tesla','TSLA',711.2);
INSERT INTO stockprices(stockTicker,tickerName, price) values ('Walmart','WLT',148.96);


-- Portfolio DB
DROP TABLE IF EXISTS portfolio;
CREATE TABLE portfolio(
id int auto_increment primary key,
stockTicker varchar(50),
quantity int,
valueAtCost double,
profitAndLoss double
);

INSERT INTO portfolio(id, stockTicker, quantity,valueAtCost, profitAndLoss) values (1,'C',20,1400,60);
INSERT INTO portfolio(id, stockTicker, quantity,valueAtCost, profitAndLoss) values (2,'WFC',20,580,418.6);
INSERT INTO portfolio(id, stockTicker, quantity,valueAtCost, profitAndLoss) values (3,'GS',20,8260,2.399999999999636);
INSERT INTO portfolio(id, stockTicker, quantity,valueAtCost, profitAndLoss) values (4,'JPM',20,3000,218.39999999999964);
INSERT INTO portfolio(id, stockTicker, quantity,valueAtCost, profitAndLoss) values (5,'MS',20,1980,100.80000000000018);
INSERT INTO portfolio(id, stockTicker, quantity,valueAtCost, profitAndLoss) values (6,'BAC',20,1045.0,-200.0);
