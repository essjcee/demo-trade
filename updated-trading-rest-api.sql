CREATE DATABASE csv_trading_rest_api;

use csv_trading_rest_api;

DROP table csv_stockdata;

CREATE TABLE csv_stockdata(
date date,
ticker varchar(10),
open double,
high double,
low double,
close double,
volume bigint
);

SHOW VARIABLES LIKE "secure_file_priv";

SELECT * from csv_stockdata;

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/GS.csv'
INTO TABLE csv_stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/C.csv'
INTO TABLE csv_stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/JPM.csv'
INTO TABLE csv_stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/MS.csv'
INTO TABLE csv_stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/BAC.csv'
INTO TABLE csv_stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;

LOAD DATA INFILE 'C:/ProgramData/MySQL/MySQL Server 8.0/Uploads/WFC.csv'
INTO TABLE csv_stockdata
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS;
    
SELECT * from csv_stockdata;

ALTER TABLE csv_stockdata
ADD statusCode INT DEFAULT 0;

ALTER TABLE csv_stockdata
ADD buyOrSell varchar(10);

update csv_stockdata
    set buyOrSell = elt(floor(rand()*2) + 1, 'BUY', 'SELL');
    
SELECT * from csv_stockdata
WHERE ticker='GS' ORDER BY date;