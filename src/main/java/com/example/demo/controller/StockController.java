package com.example.demo.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.entities.Stock;
import com.example.demo.entities.Portfolio;
import com.example.demo.entities.StockPrices;
import com.example.demo.service.StockService;
@RequestMapping("api/stocks")
@CrossOrigin
@RestController
public class StockController {

	@Autowired
	StockService service;
	
	//get trading history of user
	@GetMapping(value="/history")
	public List<Stock> getTradingHistory(){
		return service.getTradingHistory();
	}
	@GetMapping(value="/prices")
	public List<StockPrices> getStockPrices(){
		return service.getStockPrices();
	}
	@GetMapping(value="/portfolio")
	public List<Portfolio> getPortfolio(){
		return service.getPortfolio();
	}
	
	@PostMapping(value = "/")
	public Stock addStock(@RequestBody Stock stock) {
		return service.newStock(stock);
	}


	/*
	// gets all stocks in database
	@GetMapping(value = "/")
	public List<Stock> getAllStocks() {
		return service.getAllStocks();
	}

	// gets all stocks of an organization specified by ticker
	@GetMapping(value = "/tic/{ticker}")
	public List<Stock> getStockViaTickerOnly(@PathVariable("ticker") String ticker) {
		return service.getStockViaTickerOnly(ticker);
	}

	// gets stocks on a particular day of an organization specified by ticker
	@GetMapping(value = "/tic/date/{ticker}/{date}")
	public List<Stock> getStockViaTickerDate(@PathVariable("ticker") String ticker, @PathVariable("date") String date) {
		return service.getStockViaTicker(ticker, date);
	}

	// adds a new stock to the database

	 //deletes an existing stock from the database
	@DeleteMapping(value = "/{ticker}/{date}")
	public String deleteStock(@PathVariable String ticker, @PathVariable("date") String date) {
		return service.deleteStock(ticker, date);
	}
	*/
}

