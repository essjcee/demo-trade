package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradingRestApiApplication {
	public static void main(String[] args) {

		// entry-point for Spring Boot application
		SpringApplication.run(TradingRestApiApplication.class, args);
	}

}
