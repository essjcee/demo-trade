package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entities.Stock;
import com.example.demo.entities.Portfolio;
import com.example.demo.entities.StockPrices;
import com.example.demo.repository.StockRepository;

@Service
public class StockService {

	@Autowired
	private StockRepository repository;
	

	public List<Stock> getTradingHistory() {
		return repository.getTradingHistory();
	}

	public List<Portfolio> getPortfolio() {
		return repository.getPortfolio();
	}
	public List<StockPrices> getStockPrices() {
		return repository.getStockPrices();
	}
	public Stock newStock(Stock stock) {
		return repository.addStock(stock);
	}
	
	/*
	public List<Stock> getStockViaTickerOnly(String ticker) {
		return repository.getStockByTickerOnly(ticker);
	}

	public List<Stock> getStockViaTicker(String ticker, String date) {
		return repository.getStockByTicker(ticker, date);
	}

	public String deleteStock(String ticker, String date) {
		return repository.deleteStock(ticker, date);
	}
	*/

}
