package com.example.demo.entities;

public class Portfolio {
	String stockTticker;
	public String getStockTticker() {
		return stockTticker;
	}
	public void setStockTticker(String stockTticker) {
		this.stockTticker = stockTticker;
	}
	public long getQuantity() {
		return quantity;
	}
	public void setQuantity(long quantity) {
		this.quantity = quantity;
	}
	public double getValueAtCost() {
		return valueAtCost;
	}
	public void setValueAtCost(double valueAtCost) {
		this.valueAtCost = valueAtCost;
	}
	public double getpAndL() {
		return pAndL;
	}
	public void setpAndL(double pAndL) {
		this.pAndL = pAndL;
	}
	int id;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	long quantity;
	double valueAtCost;
	double pAndL;
	public Portfolio(int id,String stockTticker, long quantity, double valueAtCost, double pAndL) {
		super();
		this.id = id;
		this.stockTticker = stockTticker;
		this.quantity = quantity;
		this.valueAtCost = valueAtCost;
		this.pAndL = pAndL;
	}
	
}
