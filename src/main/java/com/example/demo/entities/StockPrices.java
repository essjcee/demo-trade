package com.example.demo.entities;

public class StockPrices {
	String tickerName;
	String stockticker;
	double price;
	public String getTickerName() {
		return tickerName;
	}
	public void setTickerName(String tickerName) {
		this.tickerName = tickerName;
	}
	public String getStockticker() {
		return stockticker;
	}
	public void setStockticker(String stockticker) {
		this.stockticker = stockticker;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public StockPrices() {
		
	}
	public StockPrices(String tickerName, String stockticker, double price) {
		super();
		this.tickerName = tickerName;
		this.stockticker = stockticker;
		this.price = price;
	}
	

}
