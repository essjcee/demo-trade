package com.example.demo.repository;

import java.util.List;

import com.example.demo.entities.Stock;
import com.example.demo.entities.Portfolio;
import com.example.demo.entities.StockPrices;

public interface StockRepository {
	

	// interface that defines all methods containing business logic
	public List<Stock> getTradingHistory();	
	public List<Portfolio> getPortfolio();	
	public List<StockPrices> getStockPrices();
	public Stock addStock(Stock stock);
	
	/*

	public List<Stock> getStockByTickerOnly(String ticker);

	public List<Stock> getStockByTicker(String stockTicker, String date);

	public String deleteStock(String ticker, String date);
	*/

}

