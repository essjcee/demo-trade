package com.example.demo.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.demo.entities.Stock;
import com.example.demo.entities.StockPrices;
import com.example.demo.entities.Portfolio;

@Repository
public class MySQLStockRepository implements StockRepository {

	@Autowired
	JdbcTemplate template;

	// methods with business logic to GET, POST, PUT, DELETE stock data
	
	@Override
	public List<Stock> getTradingHistory() {
		String sql = "SELECT * FROM stockdata";
		return template.query(sql, new StockRowMapper());
	}
	
	@Override
	public List<StockPrices> getStockPrices(){
		String sql = "SELECT * FROM stockprices";
		return template.query(sql, new StockPricesRowMapper());
	}
	
	@Override
	public List<Portfolio> getPortfolio() {
		String sql = "select * from portfolio;";	
		return template.query(sql, new PortfolioRowMapper());
	}
	
	
	@Override
	public Stock addStock(Stock stock) {
		String sql = "INSERT INTO stockdata(date, stockTicker, price, quantity, buyOrSell, statusCode) "
				+ "VALUES(?,?,?,?,?,?)";
		template.update(sql, stock.getDate(), stock.getStockTicker(),stock.getPrice(), stock.getQuantity(),
				stock.getBuyOrSell(), stock.getStatusCode());
		return stock;
	}
	
	
	/*
	@Override
	public List<Stock> getStockByTickerOnly(String ticker) {
		String sql = "SELECT date, ticker, open, high, low, close, volume, statusCode, buyOrSell FROM csv_stockdata "
				+ "WHERE ticker = ?";
		return template.query(sql, new StockRowMapper(), ticker);
	}

	@Override
	public List<Stock> getStockByTicker(String ticker, String date) {
		String sql = "SELECT date, ticker, open, high, low, close, volume, statusCode, buyOrSell FROM csv_stockdata "
				+ "WHERE ticker = ? AND date = ?";
		return template.query(sql, new StockRowMapper(), ticker, date);
	}

	@Override
	public String deleteStock(String ticker, String date) {
		String sql = "DELETE FROM csv_stockdata WHERE ticker = ? AND date = ?";
		template.update(sql, ticker, date);
		return ticker;
	}

	@Override
	public Stock addStock(Stock stock) {
		String sql = "INSERT INTO csv_stockdata(date, ticker, open, high, low, close, volume, statusCode, buyOrSell) "
				+ "VALUES(?,?,?,?,?,?,?,?,?)";
		template.update(sql, stock.getDate(), stock.getTicker(), stock.getOpen(), stock.getHigh(), stock.getLow(),
				stock.getClose(), stock.getVolume(), stock.getStatusCode(), stock.getBuyOrSell());
		return stock;
	}
	*/

}

class StockRowMapper implements RowMapper<Stock> {

	@Override
	public Stock mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Stock(rs.getInt("id"),rs.getString("date"), rs.getString("stockTicker"), rs.getDouble("price"),
				rs.getLong("quantity"), rs.getString("buyOrSell"),rs.getInt("statusCode"));
	}
	

}

class PortfolioRowMapper implements RowMapper<Portfolio> {

	@Override
	public Portfolio mapRow(ResultSet rs, int rowNum) throws SQLException {
		return new Portfolio(rs.getInt("id"),rs.getString("stockTicker"),rs.getInt("quantity"),rs.getDouble("valueAtCost"), rs.getDouble("profitAndLoss"));
	}

}
class StockPricesRowMapper implements RowMapper<StockPrices> {

	@Override
	public StockPrices mapRow(ResultSet rs, int rowNum) throws SQLException {
		// setpAndL(double pAndL)	
		return new StockPrices(rs.getString("tickerName"),rs.getString("stockTicker"),rs.getDouble("price"));
	}
	

}


