package com.conygre.training.ordersimulator.dao;

import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;


/**
 * This class is a test harness class - it only exists for testing.
 * 
 * This is SIMULATING sending trade orders to an exchange for them to be filled.
 * 
 * This class uses the following codes to represent the state of an order:
 * 
 * 0 : initialized i.e. has not yet been processed at all
 * 1 : processing  i.e. the order has been sent to an exchange, we are waiting for a response
 * 2 : filled i.e. the order was successfully placed
 * 3 : rejected i.e. the order was not accepted by the trading exchange
 * 
 * The above are JUST SUGGESTED VALUES, you can change or improve as you see think is appropriate.
 */
@Repository
public class OrderProcessingSimulator {
	
	// Standard mechanism for logging with spring boot - org.slf4j is built-in to spring boot
	private static final Logger LOG = LoggerFactory.getLogger(OrderProcessingSimulator.class);

	// You'll need to change these to match whatever you called your table and status_code field
	// You may also need to change the database name in application-mysql.properties and application-h2.properties
	// The default database name that's used here is "appDB"
	private static final String TABLE = "stockdata";
	private static final String PORTFOLIO_TABLE = "portfolio";
	private static final String CURRENT_PRICE_TABLE = "stockprices";
	private static final String STATUS_CODE = "statusCode";

	private int percentFailures = 10;

	@Autowired
	private JdbcTemplate template;

	/**
	 * Any record in the configured database table with STATE=0 (init)
	 * 
	 * Will be changed to STATE=1 (processing)
	 */
	@Scheduled(fixedRateString = "${scheduleRateMs:10000}")
	public int findTradesForProcessing() {
		String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=1 WHERE " + STATUS_CODE + "=0";
		int numberChanged = template.update(sql);
		
		LOG.debug("Updated [" + numberChanged + "] order from initialized (0) TO processing (1)");
		
		return numberChanged;
	}
	
	/**
	 * Anything in the configured database table with STATE=1 (processing)
	 * 
	 * Will be changed to STATE=2 (filled) OR STATE=3 (rejected)
	 * 
	 * This method uses a random number to determine when trades are rejected.
	 */
	@Scheduled(fixedRateString = "${scheduleRateMs:15000}")
	public int findTradesForFillingOrRejecting() {
		int totalChanged = 0;
		int lastChanged = 0;

		do {
			lastChanged = 0;
			
			int randomInteger = new Random().nextInt(100);

			//LOG.debug("Random number is [" + randomInteger +
					 // "] , failure rate is [" + percentFailures + "]");
			
			// use a random number to decide if we'll simulate success OR failure
			if(randomInteger > percentFailures) {
				// Mark this one as success
				lastChanged = markTradeAsSuccess();
				//LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO success (2)");
			}
			else {
				// Mark this one as failure!!
				lastChanged = markTradeAsFailure();
				//LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO failure (3)");
			}
			totalChanged += lastChanged;

		} while (lastChanged > 0);

		return totalChanged;
	}

	/*
	 * Update a single record to success or failure
	 */
	private int markTradeAsFailure() {
		
		String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=3" +
	                  " WHERE " + STATUS_CODE + "=1 limit 1";
		LOG.debug("Updated [" + "] order from processing (1) TO failure (3)");
		
		return template.update(sql);
	}
	
	private int markTradeAsSuccess() {
	
		try {
			
			String sql1 = "select id FROM " +TABLE +" where " + STATUS_CODE + "=1 limit 1";
			int id = template.queryForObject(sql1,Integer.class);
			
			String sql2 = "select buyOrSell FROM " +TABLE +" where id="+id;
			String buyOrSell = template.queryForObject(sql2, String.class);
			
			String sql3 = "select stockTicker FROM " +TABLE +" where id="+id;
			String ticker = template.queryForObject(sql3, String.class);
			
			String sql4 = "select quantity FROM " +TABLE +" where id="+id;
			int quantity = template.queryForObject(sql4, Integer.class);
			

			String sql5 = "select price FROM " +TABLE +" where id="+id;
			double buyPrice = template.queryForObject(sql5, Double.class);
			
			if(buyOrSell.equals("Buy")) {
				LOG.debug("over here "+ id);
				boughtUpdatePortfolio(id, ticker, quantity, buyPrice);
			}
			else {
				boolean canSell[] = new boolean[1];
				canSell[0] = true;
				LOG.debug("sell me jaa");
				sellUpdatePortfolio(id, ticker, quantity, buyPrice, canSell);
				if(!canSell[0]) {
					LOG.debug("nahi bikega");
					return markTradeAsFailure();
				}
			}
			
			String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=2" +
	                " WHERE id="+id;
			LOG.debug("here");
			return template.update(sql);
			
		}
		catch(Exception e) {
			return 0;
		}
		
	}
	
	private void sellUpdatePortfolio(int id, String ticker, int sellQuantity, double sellPrice, boolean[] canSell) {
		LOG.debug("sellUpdatePortfolio");
		int currQuantity;
		try {
			String sql1 = "select quantity from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
			currQuantity = template.queryForObject(sql1,Integer.class);
		}
		catch(Exception e) {
			canSell[0] = false;
			return;
		}
		LOG.debug("sellUpdatePortfolio"+currQuantity);
		
		if(currQuantity<sellQuantity) {
			LOG.debug("kese bechu bhai");
			canSell[0] = false;
		}
		else if(currQuantity==sellQuantity) {
			LOG.debug("pura maal khali kar");
			deletePortfolioRow(ticker);
		}
		else {
			LOG.debug("thoda rehne de bhai");
			sellUpdateRowPortfolio(ticker, sellQuantity, sellPrice);
		}
	}
	
	private void sellUpdateRowPortfolio(String ticker, int sellQuantity, double sellPrice) {
		LOG.debug("thoda rehne de bhai");
		String sql1 = "select quantity from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
		int oldQuantity = template.queryForObject(sql1,Integer.class);
		int newQuantity = oldQuantity - sellQuantity;
		
		String sql2 = "select valueAtCost from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
		double oldValueAtCost = template.queryForObject(sql1,Double.class);
		double newValueAtCost = (double)newQuantity*(oldValueAtCost/(double)oldQuantity);
		
		String sql3 = "select price from " + CURRENT_PRICE_TABLE + " where tickerName=\""+ticker+"\"";
		double currPrice = template.queryForObject(sql1, Double.class);
		
		double profitAndLoss = ((double)newQuantity)*currPrice-newValueAtCost;
		
		String updateSql = "update " + PORTFOLIO_TABLE + " set quantity=?, valueAtCost=?, profitAndLoss=?"
				+ " where stockTicker=\""+ticker+"\"";
		template.update(updateSql, newQuantity, newValueAtCost, profitAndLoss);
		
	}
	
	private void deletePortfolioRow(String ticker) {
		LOG.debug("deletePortfolioRow");
		String sql1 = "delete from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
		template.update(sql1);
	}
	
	private void  boughtUpdatePortfolio(int id, String ticker, int quantity, double buyPrice) {

		String sql1 = "select count(*) from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
		int count = template.queryForObject(sql1,Integer.class);

		if(count==0){
			addRowPortfolio(id, ticker, quantity, buyPrice);
		}
		else {
			updateRowPortfolio(id,ticker, quantity, buyPrice);
		}
		
	}
	
	private void addRowPortfolio(int id, String ticker, int quantity, double buyPrice) {

		double valueAtCost = ((double)quantity)*buyPrice;
		
		String sql1 = "select price from " + CURRENT_PRICE_TABLE + " where tickerName=\""+ticker+"\"";
		double currPrice = template.queryForObject(sql1, Double.class);
		
		double valueCurrent = ((double)quantity)*currPrice;
		double profitAndLoss = valueCurrent - valueAtCost;
		
		LOG.debug("addRowPortfolio "+ profitAndLoss);
		
		String insertSql = "insert into " + PORTFOLIO_TABLE + " (stockTicker, quantity, valueAtCost, profitAndLoss)"
					+ " values(?,?,?,?)";
		LOG.debug(insertSql);
		template.update(insertSql, ticker, quantity, valueAtCost, profitAndLoss);
		
	}
	
	private void updateRowPortfolio(int id, String ticker, int quantity, double buyPrice) {

		double currValueAtCost = ((double)quantity)*buyPrice;
		
		String sql1 = "select quantity from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
		int oldQuantity = template.queryForObject(sql1, Integer.class);
		int newQuantity = quantity + oldQuantity;
		
		String sql2 = "select valueAtCost from " + PORTFOLIO_TABLE + " where stockTicker=\""+ticker+"\"";
		double oldValueAtCost = template.queryForObject(sql2, Double.class);
		double newValueAtCost = currValueAtCost + oldValueAtCost;

		
		String sql3 = "select price from " + CURRENT_PRICE_TABLE + " where tickerName=\""+ticker+"\"";
		double currPrice = template.queryForObject(sql3, Double.class);

		
		double valueCurrent = ((double)newQuantity)*currPrice;
		double profitAndLoss = valueCurrent - newValueAtCost;

		
		String updateSql = "update " + PORTFOLIO_TABLE + " set quantity=?, valueAtCost=?, profitAndLoss=?"
				+ " where stockTicker=\""+ticker+"\"";
		template.update(updateSql, newQuantity, newValueAtCost, profitAndLoss);
	}
	
	

	public void setPercentFailures(int percentFailures) {
		this.percentFailures = percentFailures;
	}

}